const dotenv = require('dotenv').config({ path: `${__dirname}/.env` });
const fs = require('fs');
const Fibery = require('fibery-unofficial');

// TODO: Slack alert

// TODO: if a new Tool has been added since the last sync,
// TODO: find all Leads synced before the Tool existed and count the Tools mentions


async function getToolMapping(fibery) {
    const tools = await fibery.entity.query({
        'q/from': 'Crm/Tool',
        'q/select': [
            'fibery/id',
            'Crm/name',
            'Crm/Other names (comma-separated)'
        ],
        'q/limit': 'q/no-limit'
    });

    return tools.reduce((mapping, tool) => {
        const otherNames = tool['Crm/Other names (comma-separated)']
            ? tool['Crm/Other names (comma-separated)'].split(',').map(name => name.trim())
            : [];
        [tool['Crm/name'], ...otherNames].forEach(name => mapping[name.toLowerCase()] = tool['fibery/id']);
        return mapping;
    }, {});
}

async function getLeads(fibery) {
    const leads = await fibery.entity.query({
        'q/from': 'Crm/lead',
        'q/select': [
            'fibery/id',
            'user/name',
            {'user/Description': ['Collaboration~Documents/secret']},
            {
                'user/Tools': {
                    'q/select': [
                        'Crm/name'
                    ],
                    'q/limit': 'q/no-limit'
                }
            },
        ],
        'q/where': ['=', 'Crm/Tools Synced Date', null],
        'q/limit': 'q/no-limit'
    });

    const secrets = leads.map(lead => ({secret: lead['user/Description']['Collaboration~Documents/secret']}));
    const descriptions = await fibery.document.getBatch(secrets, 'md');
    const secretToContent = descriptions.reduce((obj, d) => Object.assign({[d.secret]: d.content}, obj), {});

    return leads.map(lead => ({
        id: lead['fibery/id'],
        name: lead['user/name'],
        description: secretToContent[lead['user/Description']['Collaboration~Documents/secret']],
        tools: lead['user/Tools'].map(tool => tool['Crm/name'])
    }));
}

function extractTools(lead, toolToId) {
    if (!lead.description) {
        return null;
    }

    const tools = Object.keys(toolToId).filter(tool => lead.description.toLowerCase().includes(tool));
    if (!tools.length) {
        return null;
    }

    return {
        'type': 'Crm/lead',
        'field': 'user/Tools',
        'entity': {'fibery/id': lead.id},
        'items': tools.map(tool => ({'fibery/id': toolToId[tool]}))
    };
}

function splitIntoBatches(array, batchSize = 50) {
    return array.reduce((resultArray, item, index) => {
        const batchIndex = Math.floor(index/batchSize);

        if(!resultArray[batchIndex]) {
            resultArray[batchIndex] = [] // start a new chunk
        }

        resultArray[batchIndex].push(item);

        return resultArray
    }, [])
}


async function update() {
    const fibery = new Fibery({host: process.env.host, token: process.env.token});

    const toolToId = await getToolMapping(fibery);
    const leads = await getLeads(fibery);
    const updates = leads.map(lead => ({
        update: {
            'type': 'Crm/lead',
            'entity': {
                'fibery/id': lead.id,
                'Crm/Tools Synced Date': (new Date()).toISOString()
            }
        },
        collectionUpdate: extractTools(lead, toolToId)
    }));

    const batches = splitIntoBatches(updates);
    for (const batch of batches) {
        await fibery.command.executeBatch([
            ...fibery.command.updateEntityBatchCmds(batch.map(b => b.update)),
            ...fibery.command.addToEntityCollectionFieldBatchCmds(batch.filter(b => b.collectionUpdate).map(b => b.collectionUpdate))
        ]);
    }

    return updates.length;
}

function log(message, isError = false) {
    const output = isError ? `\n    ${message}` : `\n${message}`;
    fs.appendFileSync(`${__dirname}/.log`, output);
}


update()
    .then(updatesCount => log(`${new Date()}: ${updatesCount} Leads updated`))
    .catch(err => log(`${new Date()}: ${err}`, true));
