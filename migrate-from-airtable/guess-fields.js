const guessFields = (table) => {
    // in reality we either determine the fields from Airtable metadata or by guessing based on field values
    // in this example we just hardcode the mapping
    // TODO: replace hardcodes with a proper implementation and decide what to do with relation fields

    return {
        'Серии': {
            nameField: 'ID серии',
            fieldsToCreate: [
                {
                    name: 'Сезон',
                    'fibery/type': 'fibery/int'
                },
                {
                    name: 'Серия',
                    'fibery/type': 'single-select',
                    meta: {
                        options: [
                            {name: 'Весна', id: '34941540-9b5b-11e9-ac83-45d19319be05'},
                            {name: 'Лето', id: '34943c50-9b5b-11e9-ac83-45d19319be05'},
                            {name: 'Осень', id: '34943c51-9b5b-11e9-ac83-45d19319be05'},
                            {name: 'Зима', id: '34943c52-9b5b-11e9-ac83-45d19319be05'}
                        ]
                    }
                },
                {
                    name: 'Обладатель совы',
                    'fibery/type': 'fibery/text'
                },
                // {
                //     name: 'Игры',
                //     'fibery/type': 'relation',
                //     meta: {to: 'ЧГК/Игра', toName: 'ЧГК/Серия', isFromMany: false, isToMany: true}
                // }
            ]
        },
        'Игры': {
            nameField: 'ID игры',
            fieldsToCreate: [
                {
                    name: 'По счёту',
                    'fibery/type': 'fibery/int'
                },
                {
                    name: 'Дата',
                    'fibery/type': 'fibery/date'
                },
                {
                    name: 'Серия',
                    'fibery/type': 'relation',
                    meta: {to: 'ЧГК/Серия', toName: 'ЧГК/Игры', isFromMany: true, isToMany: false}
                }
            ]
        }
    }[table.name];
};

module.exports = guessFields;
