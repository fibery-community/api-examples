const getFromAirtable = require('./get-from-airtable');
const uploadToFibery = require('./upload-to-fibery');
const MOCK = true;


if (MOCK) {
    uploadToFibery(require('./airtable-mock-data'))
} else {
    getFromAirtable()
        .then(tables => uploadToFibery(tables));
}
