const dotenv = require('dotenv').config();

//TODO: replace this hardcoded mock with a proper implementation
module.exports.singularize = (name) => {
    return {
        'Серии': 'Серия',
        'Игры': 'Игра'
    }[name] || name;
};

module.exports.replaceSpaces = (name, symbol) => name.replace(new RegExp(' ', 'g'), symbol);

module.exports.log = (message) => process.env.enableLog && console.log(message);
