const dotenv = require('dotenv').config();
const log = require('./helpers').log;
const Fibery = require('fibery-unofficial');
const Transformer = require('./transformer');


const uploadToFibery = async (airTables, shouldContinueIfPossible = true) => {
    const fibery = new Fibery({host: process.env.fiberyHost, token: process.env.fiberyApiKey});
    const appName = process.env.fiberyAppName;
    const transformer = new Transformer(appName);


    const existingApp = (await fibery.getSchema()).filter(type => type['fibery/name'].startsWith(`${appName}/`));
    log('existing app retrieved');

    if (!shouldContinueIfPossible && existingApp.length) {
        await fibery.type.deleteBatch(existingApp.map(type => ({
            'name': type['fibery/name'],
            'delete-entities?': true,
            'delete-related-fields?': true
        })));
        log('existing app cleaned up');
    }


    const tables = transformer.transform(airTables);


    const types = tables.map(t => t.type);
    const existingTypes = existingApp.map(type => type['fibery/name']);
    const missingTypes = shouldContinueIfPossible
        ? types.filter(type => !existingTypes.includes(type['fibery/name']))
        : types;
    if (missingTypes.length) {
        await fibery.type.createBatch(missingTypes);
        log(`${missingTypes.length} types created`);
    }


    const fields = tables.reduce((allFields, table) => allFields.concat(table.fields), []);
    const existingFields = existingApp.reduce((acc, type) =>
            acc.concat(type['fibery/fields'].map(field => `${type['fibery/name']} ${field['fibery/name']}`)),
        []);
    const missingFields = shouldContinueIfPossible
        ? fields.filter(field => !existingFields.includes(`${field['fibery/holder-type']} ${field['fibery/name']}`))
        : fields;

    if (missingFields.length) {
        await fibery.field.createBatch(missingFields);
        log(`${missingFields.length} fields created`);
    }


    const airtableIdField = `${appName}/Airtable ID`;
    for (const table of tables) {
        table.existingEntities = shouldContinueIfPossible
            ? await fibery.entity.query({
                'q/from': table.type['fibery/name'],
                'q/select': ['fibery/id', airtableIdField],
                'q/limit': 'q/no-limit' // TODO: paging
            })
            : [];
        const existingEntitiesAirtableIds = table.existingEntities.map(entity => entity[airtableIdField]);
        table.missingEntities = shouldContinueIfPossible
            ? table.entities.filter(entity => !existingEntitiesAirtableIds.includes(entity.entity[airtableIdField]))
            : table.entities;

        if (table.missingEntities.length) {
            await fibery.entity.createBatch(table.missingEntities);
            log(`${table.missingEntities.length} ${table.name} entities created`);
        }
    }

    // TODO: correctly update relations for entities that had already existed
    for (const table of tables) {
        if (table.missingEntities.length) {
            if (table.toOneRelations.length) {
                await fibery.entity.updateBatch(table.toOneRelations);
                log(`${table.toOneRelations.length} ${table.name} to-one relations set`);
            }

            if (table.toManyRelations.length) {
                await fibery.entity.addToEntityCollectionFieldBatch(table.toManyRelations);
                log(`${table.toManyRelations.length} ${table.name} to-many relations set`);
            }
        }
    }
};

module.exports = uploadToFibery;
