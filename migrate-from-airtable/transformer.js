const uuid = require('uuid/v1');
const guessFields = require('./guess-fields');
const singularize = require('./helpers').singularize;

const Transformer = class {
    constructor(appName) {
        this._appName = appName;
        this._airtableIdToFiberyId = {};
    }

    _transformType(typeName) {
        return {
            'fibery/name': typeName,
            'fibery/meta': {
                'fibery/domain?': true,
                'fibery/secured?': true,
                'ui/color': '#F7D130' // TODO: use a color palette
            }
        }
    }

    _transformFields(typeName, airTable) {
        const guessedFields = guessFields(airTable);

        const extractFieldFn = field => record => {
            if (field.name === 'Airtable ID') {
                return record.id;
            } else if (field['fibery/type'] === 'single-select') {
                const valueToId = field.meta.options.reduce((obj, option) => {
                    return Object.assign({[option.name]: option.id}, obj);
                }, {});
                return {'fibery/id': valueToId[record[field.name]]};
            } else if (field['fibery/type'] === 'relation') {
                return field.meta.isToMany ? record[field.name] : record[field.name][0];
            } else {
                return record[field.name];
            }
        };

        const fieldsToCreate = guessedFields.fieldsToCreate.concat({
            name: 'Airtable ID',
            'fibery/type': 'fibery/text',
            'fibery/meta': {
                'fibery/secured?': false,
                'fibery/readonly?': true
            }
        }).map(field => Object.assign({
            'fibery/holder-type': typeName,
            'fibery/name': `${this._appName}/${field.name}`,
            extract: extractFieldFn(field)
        }, field));

        return {
            nameField: guessedFields.nameField,
            toCreate: fieldsToCreate
        }
    }

    _transformEntities(typeName, fields, airTable) {
        const entities = [], toOneRelations = [], toManyRelations = [];
        const RANK_DISTANCE = 10 ** 6;

        airTable.records.forEach((record, i) => {
            const id = uuid();
            this._airtableIdToFiberyId[record.id] = id;

            const entity = {
                'type': typeName,
                'entity': {
                    'fibery/id': id,
                    'fibery/rank': i * RANK_DISTANCE,
                    'fibery/name': record[fields.nameField]
                }
            };

            fields.toCreate.forEach(field => {
                if (field['fibery/type'] !== 'relation') {
                    entity.entity[field['fibery/name']] = field.extract(record);
                } else if (!field.meta.isToMany) {
                    toOneRelations.push({
                        'type': typeName,
                        'entity': {
                            'fibery/id': id,
                            [field['fibery/name']]: field.extract(record)
                        }
                    });
                } else {
                    toManyRelations.push({
                        'type': typeName,
                        'field': field['fibery/name'],
                        'entity': {'fibery/id': id},
                        'items': field.extract(record)
                    })
                }
            });

            entities.push(entity);
        });


        return {
            entities,
            toOneRelations,
            toManyRelations
        }
    }

    _replaceWithFiberyId(airtableId) {
        return {'fibery/id': this._airtableIdToFiberyId[airtableId]}
    }

    transformTable(airTable) {
        const typeName = `${this._appName}/${singularize(airTable.name)}`;
        const type = this._transformType(typeName);

        const transformedFields = this._transformFields(typeName, airTable);
        const transformedEntities = this._transformEntities(typeName, transformedFields, airTable);

        return {
            name: airTable.name,
            type,
            fields: transformedFields.toCreate,
            entities: transformedEntities.entities,
            toOneRelations: transformedEntities.toOneRelations,
            toManyRelations: transformedEntities.toManyRelations
        }
    }

    transform(airTables) {
        return airTables
            .map(airTable => this.transformTable(airTable))
            .map(transformedTable => {
                const toOneRelations = transformedTable.toOneRelations.map(relation => {
                    const updated = Object.assign({}, relation);
                    const field = Object.keys(relation.entity).filter(k => k !== 'fibery/id')[0];
                    updated.entity[field] = this._replaceWithFiberyId(relation.entity[field]);
                    return updated;
                });

                const toManyRelations = transformedTable.toManyRelations.map(relation => {
                    return Object.assign({}, relation,
                        {items: relation.items.map(airtableId => this._replaceWithFiberyId(airtableId))}
                    );
                });

                return Object.assign({}, transformedTable, {toOneRelations, toManyRelations});
            });
    }
};

module.exports = Transformer;
