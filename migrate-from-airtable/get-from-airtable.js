const dotenv = require('dotenv').config();
const Airtable = require('airtable');

const base = new Airtable({ apiKey: process.env.airtableApiKey }).base(process.env.airtableBaseId);
const tableNames = process.env.airtableTableNames.split(',');

const getFromAirtable = () => {
    return Promise.all(tableNames.map(t => {
        return base.table(t).select().all().then(recordsRaw => {
            const recordsParsed = recordsRaw.map(r => {
                const fields = Object.keys(r.fields);
                return fields.reduce((result, f) => Object.assign(result, { [f]: r.get(f) }), { id: r.id });
            });

            return {
                name: t,
                records: recordsParsed
            };
        });
    }));
};

module.exports = getFromAirtable;
