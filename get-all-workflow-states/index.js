const dotenv = require('dotenv').config({ path: `${__dirname}/.env` });
const Fibery = require('fibery-unofficial');

const fs = require('fs');
const fastcsv = require('fast-csv');

async function getWorkflowStates() {
    const workspace = process.env.host;

    const fibery = new Fibery({host: process.env.host, token: process.env.token});
    const schema = await fibery.getSchema();

    const workflowTypes = schema
        .map(t => t['fibery/name'])
        .filter(n => n.startsWith('workflow/')
            && ! n.endsWith('_deleted')
            && n !== 'workflow/workflow');

    const csvRows = [];

    for (const workflowType of workflowTypes) {
        const workflowStates = await fibery.entity.query({
            'q/from': workflowType,
            'q/select': [
                'fibery/id',
                'enum/name',
                'workflow/Type'
            ],
            'q/order-by': [
                [['fibery/rank'], "q/asc"],
            ],
            'q/limit': `q/no-limit`
        });

        for (const workflowState of workflowStates) {
            csvRows.push({
                workspace: workspace,
                type: workflowType,
                id: workflowState['fibery/id'],
                name: workflowState['enum/name'],
                state_type: workflowState['workflow/Type']
            });
        }
    }

    const writableStream = fs.createWriteStream(`states-${workspace.replace('.fibery.io', '')}.csv`);
    fastcsv
        .write(csvRows, { headers: true })
        .pipe(writableStream)
        .on('finish', () => {
            console.log(`CSV file successfully saved`);
        });

}

getWorkflowStates()

