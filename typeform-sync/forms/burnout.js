module.exports = {
    formId: 'DP0h0w',
    fiberyIdField: 'burnout_id',
    fiberyDescriptionField: 'user/Description',
    fiberySyncedDateField: "Crm/Form Synced Date",
    query: {
        "q/from": "Crm/lead",
        "q/select": [
            "fibery/id",
            "Crm/Website",
            "Crm/Company size",
            {"Crm/Remote": ["enum/name"]},
            {"apps": ["q/count", ["user/Relevant Apps", "fibery/id"]]},
            {'user/Description': ['Collaboration~Documents/secret']}
        ],
        "q/where": [
            "q/and",
            ["=", ["user/Campaign", "fibery/id"], "$campaign"],
            ["=", ["Crm/Form?"], "$form"],
            ['=', 'Crm/Form Synced Date', null]
        ],
        "q/limit": "q/no-limit"
    },
    params: {
        "$campaign": "5d156b90-c5b8-11e9-ab9d-45f31c61e67f",
        "$form": true
    },
    questions: {
        'etqsiGWXHSQS': {
            text: 'What is your startup\'s website?',
            filter: (entity, answer) => !entity["Crm/Website"] && answer,
            generateUpdate: answer => ({
                type: "update",
                value: {"Crm/Website": answer}
            })
        },
        'RtvTL4CJhFmh': {
            text: 'How many people does your team consist of?',
            filter: (entity, answer) => !entity["Crm/Company size"] && answer,
            generateUpdate: answer => ({
                type: "update",
                value: {"Crm/Company size": answer}
            })
        },
        'Nya3uJf2agaD': {
            text: 'Are you working remotely?',
            filter: (entity, answer) => !entity["Crm/Remote"]["enum/name"] && answer,
            generateUpdate: answer => {
                const REMOTE_IDS = {
                    'Mostly': 'e635ef30-c5a9-11e9-ab9d-45f31c61e67f',
                    'Sometimes': 'e635ef31-c5a9-11e9-ab9d-45f31c61e67f',
                    'Almost never': 'e635ef32-c5a9-11e9-ab9d-45f31c61e67f'
                };

                return {
                    type: "addToEntityCollectionField",
                    value: {
                        "field": "Crm/Remote",
                        "items": [{"fibery/id": REMOTE_IDS[answer]}]
                    }
                }
            }
        },
        'w0GojuFNjbeq': {
            text: 'Which workflows do you go through regularly?',
            filter: (entity, answer) => entity["apps"] === 0 && answer.length,
            generateUpdate: answer => {
                const APP_IDS = {
                    'Customer discovery': 'd1c2b490-b210-11e9-b0bb-4cb55ee71c8f',
                    'Product management': '929ec000-d918-11e8-9ed3-bbb8a875e566',
                    'Software development': '0ba95790-d918-11e8-9ed3-bbb8a875e566',
                    'CRM': '02988200-6f65-11e9-919c-cb06e26953d9',
                    'Customer success': '983b1b70-70db-11e9-ae54-e341f3233377',
                    'Content production': 'd9edca30-2a0a-11e9-9eb9-99f995719775',
                    'Goal-setting (OKR)': '23978d30-dd10-11e8-81c3-1d236e0db685',
                    'Fundraising': 'f0910710-6fde-11e9-a2b6-95f43b56bb47',
                    'Hiring': '88745d10-d918-11e8-9ed3-bbb8a875e566',
                    'Vacation tracking': '9d1ebd00-d918-11e8-9ed3-bbb8a875e566'
                };

                return {
                    type: "addToEntityCollectionField",
                    value: {
                        "field": "user/Relevant Apps",
                        "items": answer.map(a => ({"fibery/id": APP_IDS[a]}))
                    }
                }
            }
        },
        'Qnj1YPkGRYpd': {
            text: 'What tools do you currently use?\n' +
                'What is not working with them?',
            filter: (entity, answer) => false,
            generateUpdate: answer => null
        },
        'wNgTHE5PdBXA': {
            text: 'Do you have a question for us?\n' +
                'Please ask here and we\'ll get back to you.',
            filter: (entity, answer) => false,
            generateUpdate: answer => null
        }
    }
};
