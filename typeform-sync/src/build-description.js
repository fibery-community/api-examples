function formatAnswer(answer) {
    switch (answer.type) {
        case 'choices':
            return answer.value.join('\n');
        case 'url':
            return `[${answer.value}](${answer.value})`;
        default:
            return answer.value.toString()
    }
}

module.exports = function (form, answers) {
    return answers.reduce((description, answer, i) => {
        const text = `**${form.questions[answer.id].text}**\n${formatAnswer(answer)}\n`;
        return i === 0 ? text : description + `\n${text}`;
    }, '');
};