//const mapping = require('./mapping');
const parseAnswer = require('./typeform').parseAnswer;
const buildDescription = require('./build-description');


module.exports = function (form, responses, entityMap) {
    return responses.reduce((acc, response) => {
        if (!response.hidden) {
            return acc;
        }

        const fiberyId = response.hidden[form.fiberyIdField]; //|| mapping[response.hidden.email];
        if (!fiberyId) {
            return acc;
        }

        const entity = entityMap[fiberyId];
        if (!entity) {
            return acc;
        }

        const entityUpdates = [], collectionUpdates = [];
        const parsedAnswers = response.answers.map(answer => ({
            id: answer.field.id,
            type: answer.type,
            value: parseAnswer(answer)
        }));
        parsedAnswers.forEach(answer => {
            if (answer.id in form.questions && form.questions[answer.id].filter(entity, answer.value)) {
                const update = form.questions[answer.id].generateUpdate(answer.value);
                if (update.type === 'addToEntityCollectionField') {
                    collectionUpdates.push(Object.assign({
                        "type": form.query["q/from"],
                        "entity": {"fibery/id": entity["fibery/id"]}
                    }, update.value));
                } else if (update.type === 'update') {
                    entityUpdates.push(update.value)
                }
            }
        });

        const entityUpdate = entityUpdates.length && {
            "type": form.query["q/from"],
            "entity": {
                "fibery/id": entity["fibery/id"],
                ...entityUpdates.reduce((obj, u) => Object.assign(obj, u), {})
            }
        };

        const descriptionUpdate = entity.description ? null : buildDescription(form, parsedAnswers);

        return acc.concat({
            entity,
            entityUpdate,
            collectionUpdates,
            descriptionUpdate
        });
    }, []);
};