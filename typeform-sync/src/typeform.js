module.exports.getResponses = async function (typeform, formId) {

    const result = await typeform.responses.list({
        uid: formId,
        completed: true,
        since: (new Date(new Date().getTime()-(21*24*60*60*1000))).toISOString(),
        pageSize: 1000
    });

    if (!result || !result.items) {
        throw new Error('cannot get responses from Typeform');
    }

    return result.items;
};

module.exports.parseAnswer = function (answer) {
    if (!answer) {
        throw new Error(`cannot parse an answer (${JSON.stringify(answer)})`);
    }

    if (answer.type === 'choice') {
        return answer.choice.label;
    } else if (answer.type === 'choices') {
        return answer.choices.labels;
    } else {
        return answer[answer.type];
    }
};
