const log = require('./helpers').log;


module.exports.getEntityMap = async function (fibery, query, params, descriptionField) {
    const entities = await fibery.entity.query(query, params);

    const secrets = entities.map(entity => ({secret: entity[descriptionField]['Collaboration~Documents/secret']}));
    const descriptions = await fibery.document.getBatch(secrets, 'md');
    const secretToContent = descriptions.reduce((obj, d) => Object.assign({[d.secret]: d.content}, obj), {});

    return entities.reduce((obj, entity) => {
        const secret = entity[descriptionField]['Collaboration~Documents/secret'];
        return Object.assign({
            [entity["fibery/id"]]: Object.assign({
                secret,
                description: secretToContent[secret]
            }, entity)
        }, obj)
    }, {});
};

module.exports.updateEntities = async function (fibery, form, updates) {
    for (const update of updates) {
        try {
            await Promise.all([
                update.entityUpdate && fibery.entity.updateBatch([update.entityUpdate]),
                update.collectionUpdates.length && fibery.entity.addToEntityCollectionFieldBatch(update.collectionUpdates),
                update.descriptionUpdate && fibery.document.update(update.entity.secret, update.descriptionUpdate, 'md')
            ]);

            await fibery.entity.updateBatch([{
                "type": form.query["q/from"],
                "entity": {
                    "fibery/id": update.entity["fibery/id"],
                    [form.fiberySyncedDateField]: (new Date()).toISOString()
                }
            }]);
        } catch (error) {
            log(`failed to update an entity ${JSON.stringify(update.entity)} (${error})`, true);
        }
    }
};