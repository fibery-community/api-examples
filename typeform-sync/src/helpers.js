const fs = require('fs');


module.exports.log = function(message, isError = false) {
    const output = isError ? `\n    ${new Date()}: ${message}` : `\n${new Date()}: ${message}`;
    fs.appendFileSync(`${__dirname}/../.log`, output);
};
