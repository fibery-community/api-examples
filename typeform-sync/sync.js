require('dotenv').config({path: `${__dirname}/.env`});
const log = require('./src/helpers').log;

const forms = require('./forms');

const createTypeformClient = require('@typeform/api-client').createClient;
const getTypeformResponses = require('./src/typeform').getResponses;

const Fibery = require('fibery-unofficial');
const getFiberyEntityMap = require('./src/fibery').getEntityMap;
const updateFiberyEntities = require('./src/fibery').updateEntities;

const prepareUpdates = require('./src/prepare-updates');

// TODO: responses with email but without fibery/id, leads will be created later


async function syncForm(typeform, fibery, form) {
    const [responses, entityMap] = await Promise.all([
        getTypeformResponses(typeform, form.formId),
        getFiberyEntityMap(fibery, form.query, form.params, form.fiberyDescriptionField)
    ]);

    const updates = prepareUpdates(form, responses, entityMap);
    await updateFiberyEntities(fibery, form, updates);
    return updates.length;
}

async function sync() {
    log('-----');
    const typeform = createTypeformClient({token: process.env.typeformToken});
    const fibery = new Fibery({host: process.env.fiberyHost, token: process.env.fiberyToken});

    let formsSynced = 0;
    for (const form of forms) {
        try {
            log(`updating form ${form.formId}...`);
            const updatesCount = await syncForm(typeform, fibery, form);
            formsSynced++;
            log(`${updatesCount} entities updated`);
        } catch (error) {
            log(`sync failed (${error})`, true);
        }
    }

    return formsSynced;
}

sync()
    .then(formsSynced => log(`${formsSynced} forms synced`))
    .catch(error => log(`sync failed (${error})`, true));
