const dotenv = require('dotenv').config();
const Fibery = require('fibery-unofficial');
const fibery = new Fibery({host: process.env.host, token: process.env.token});


async function getSchema() {
    const schema = await fibery.getSchema();
    console.log(schema);
}

async function createType() {
    const result = await fibery.type.createBatch([
        {
            'fibery/name': 'Cricket/Player',
            'fibery/meta': {
                'fibery/domain?': true,
                'fibery/secured?': true,
                'ui/color': '#F7D130'
            },
            'fibery/fields': [
                {
                    "fibery/name": 'user/salary',
                    "fibery/type": 'fibery/int',
                    "fibery/meta": {
                        "fibery/secured?": true
                    }
                }
            ]
        }
    ]);
    console.log(result);
}

async function renameType() {
    const result = await fibery.type.renameBatch([
        {
            'from-name': 'Cricket/Player',
            'to-name': 'Cricket/Bowler'
        }
    ]);
    console.log(result);
}

async function deleteType() {
    const result = await fibery.type.deleteBatch([
        {
            'name': 'Cricket/Bowler',
            'delete-entities?': true,
            'delete-related-fields?': true
        }
    ]);
    console.log(result);
}

async function createPrimitiveField() {
    const result = await fibery.field.createBatch([
        {
            'fibery/holder-type': 'Cricket/Player',
            'fibery/name': 'Cricket/Salary',
            'fibery/type': 'fibery/int'
        }
    ]);
    console.log(result);
}

async function createRelation() {
    const result = await fibery.field.createBatch([
        {
            'fibery/holder-type': 'Cricket/Player',
            'fibery/name': 'Cricket/Current~Team',
            'fibery/type': 'relation',
            meta: {
                to: 'Cricket/Team',
                toName: 'Cricket/Current~Roster',
                isFromMany: true,
                isToMany: false
            }
        }
    ]);
    console.log(result);
}

async function createSingleSelect() {
    const result = await fibery.field.createBatch([
        {
            'fibery/holder-type': 'Cricket/Player',
            'fibery/name': 'Cricket/Bowling~Hand',
            'fibery/type': 'single-select',
            meta: {
                options: [
                    {name: 'Right'},
                    {name: 'Left'}
                ]
            }
        }
    ]);
    console.log(result);
}

async function renameField() {
    const result = await fibery.field.renameBatch([
        {
            'holder-type': 'Cricket/Player',
            'from-name': 'Cricket/Salary',
            'to-name': 'Cricket/Wage'
        }
    ]);
    console.log(result);
}

async function deleteField() {
    const result = await fibery.field.deleteBatch([
        {
            'holder-type': 'Cricket/Player',
            'name': 'Cricket/Bowling~Hand',
            'delete-values?': true
        }
    ]);
    console.log(result);
}

async function getEntities() {
    const players = await fibery.entity.query({
        'q/from': 'Cricket/Player',
        'q/select': [
            'fibery/id',
            'fibery/public-id',
            'fibery/name',
            'Cricket/Full~Name',
            'Cricket/Born',
            'Cricket/Shirt~Number',
            'Cricket/Height',
            'Cricket/Retired?',

            {'Cricket/Batting~Hand': ['enum/name']},
            {'Cricket/Current~Team': ['Cricket/name']},
            {'user/Former~Teams': {'q/select': ['Cricket/name'], 'q/limit': 'q/no-limit'}},
            {'# of former teams': ['q/count', ['user/Former~Teams', 'fibery/id']]}
        ],
        'q/where': ['>=', 'Cricket/Born', '$birthday'],
        'q/order-by': [
            [['Cricket/Height'], 'q/desc'],
            [['Cricket/Born'], 'q/asc']
        ],
        'q/limit': 3
    }, {'$birthday': '1986-01-01'});

    console.log(players);
}

async function getDocument() {
    const document = await fibery.document.get('b33a25d1-99ba-11e9-8c59-09d0cb6f3aeb', 'md');
    console.log(document);
}

async function createEntity() {
    const player = await fibery.entity.createBatch([
        {
            'type': 'Cricket/Player',
            'entity': {
                'fibery/name': 'Test',
                'Cricket/Full~Name': 'Curtly Elconn Lynwall Ambrose',
                'Cricket/Born': '1963-09-21',
                'Cricket/Youth~Career': {
                    'start': '1985-01-01',
                    'end': '1986-01-01'
                },
                'Cricket/Shirt~Number': 13,
                'Cricket/Height': '2.01',
                'Cricket/Retired?': true,

                'Cricket/Batting~Hand': {'fibery/id': 'b0ed3a80-9747-11e9-9f03-fd937c4ecf3b'}
            }
        }
    ]);

    console.log(player);
}

async function updateEntity() {
    const player = await fibery.entity.updateBatch([
        {
            'type': 'Cricket/Player',
            'entity': {
                'fibery/id': '20f9b920-9752-11e9-81b9-4363f716f666',
                'Cricket/Full~Name': 'Virat \"Chikoo\" Kohli',
                'Cricket/Current~Team': {'fibery/id': 'd328b7b0-97fa-11e9-81b9-4363f716f666'}
            }
        }
    ]);

    console.log(player);
}

async function addToCollection() {
    const result = await fibery.entity.addToEntityCollectionFieldBatch([
        {
            'type': 'Cricket/Player',
            'field': 'user/Former~Teams',
            'entity': { 'fibery/id': 'd17390c4-98c8-11e9-a2a3-2a2ae2dbcce4' },
            'items': [
                { 'fibery/id': 'ab784e60-98bd-11e9-8a3a-97bdc680d7ca' },
                { 'fibery/id': '8488a7a0-98bd-11e9-8a3a-97bdc680d7ca' }
            ]
        }
    ]);

    console.log(result);
}

async function removeFromCollection() {
    const result = await fibery.entity.removeFromEntityCollectionFieldBatch([
        {
            'type': 'Cricket/Player',
            'field': 'user/Former~Teams',
            'entity': { 'fibery/id': 'd17390c4-98c8-11e9-a2a3-2a2ae2dbcce4' },
            'items': [
                { 'fibery/id': 'ab784e60-98bd-11e9-8a3a-97bdc680d7ca' },
                { 'fibery/id': '8488a7a0-98bd-11e9-8a3a-97bdc680d7ca' }
            ]
        }
    ]);

    console.log(result);
}

async function updateDocument() {
    const content = 'Virat Kohli (born 5 November 1988) is an Indian [cricketer](https://en.wikipedia.org/wiki/Cricket) who currently captains the India national team.\\nHe plays for Royal Challengers Bangalore in the Indian Premier League.';
    const result = await fibery.document.update('b33a25d1-99ba-11e9-8c59-09d0cb6f3aeb', content, 'md');

    console.log(result);
}

async function deleteEntity() {
    const result = await fibery.entity.deleteBatch([
        {
            'type': 'Cricket/Player',
            'entity': { 'fibery/id': '76d80610-9e4d-11e9-8de3-337c1ef261af' }
        }
    ]);

    console.log(result);
}

async function uploadFile() {
    const result = await fibery.file.upload('\\Users\\itiok_000\\Pictures\\virat-kohli.jpg');
    console.log(result);
}

async function downloadFile() {
    const result = await fibery.file.download('0ae665e0-9e4e-11e9-a95f-320c9d056907', './virat.jpg');
    console.log(result);
}




async function createSingleSelectTwo() {
    const result = await fibery.field.createBatch([
        {
            'fibery/holder-type': 'Domain Model/KeyResult',
            'fibery/name': 'Domain Model/achieved',
            'fibery/type': 'single-select',
            meta: {
                options: [
                    {name: 'yes'},
                    {name: 'no'}
                ]
            }
        }
    ]);
    console.log(result);
}

createSingleSelectTwo()