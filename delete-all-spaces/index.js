const dotenv = require('dotenv').config({ path: `${__dirname}/.env` });
const Fibery = require('fibery-unofficial');


async function deleteAllSpaces() {
    const fibery = new Fibery({host: process.env.host, token: process.env.token});

    const spaces = await fibery.entity.query({
        'q/from': 'fibery/app',
        'q/select': [
            'fibery/id',
            'fibery/name'
        ],
        'q/where': ['=', 'fibery/show-in-menu?', true],
        'q/limit': `q/no-limit`
    });

    if (spaces.length === 0) {
        console.log("No Spaces to delete");
    }

    for (const space of spaces) {
        await fetch(`https://${process.env.host}/api/app-gallery/json-rpc`, {
            method: 'POST',
            headers: {
                'Authorization': `Token ${process.env.token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                jsonrpc: "2.0",
                method: "uninstall",
                params: { id: space['fibery/id'] }
            })
        }).then((response) => { console.log(`"${space['fibery/name']}" is no more`) })
    }

    console.log(`${spaces.length} ${spaces.length === 1 ? 'Space' : 'Spaces'} deleted`);
}

deleteAllSpaces()